__author__='spark'

from FlowCytometryTools import FCMeasurement
from FlowCytometryTools import ThresholdGate
from glob import glob
import numpy as np
import scipy as sp
import scipy.stats
from scipy.special import erfinv
import matplotlib.pylab as pylab
import matplotlib.mlab as mlab
import matplotlib.cm as cmx
from matplotlib import cm
import matplotlib.colors as colors
from scipy import histogram
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import math

def calcConfidenceInterval(data, frac=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+frac)/2.0, n-1)
    return h

def calcBayesianCredibleRegion(data, sigma, frac=0.95):
    Nsigma = np.sqrt(2) * erfinv(frac)
    mu = data.mean()
    sigma_mu = sigma * data.size ** -0.5
    print 'data.size: ' + str(data.size)
    print 'data sigma: ' + str(sigma)
    print 'sigma mu: ' + str(sigma_mu)
    # cr_l = mu - Nsigma * sigma_mu
    # cr_u = mu + Nsigma * sigma_mu
    cr_l = mu - 2.0 * Nsigma * sigma_mu
    cr_u = mu + 2.0 * Nsigma * sigma_mu
    within_cr = (cr_l < data) & (data < cr_u)
    print 'Fraction of values in CR: {0:.3f}'.format(within_cr.sum() * 1.0 / within_cr.size)
    #return [mu - Nsigma * sigma_mu, mu + Nsigma * sigma_mu]
    #return [Nsigma * sigma_mu, Nsigma * sigma_mu]
    return Nsigma * sigma_mu

def calcCredibleInterval(xlim, ys, mass=0.95):
    """Given a range, like (0, 20), and the y values for the probability density function,
    returns the set of intervals spanning the minimum range to sum up to the mass."""

    indexed = zip(ys, range(len(ys)))
    indexed.sort(reverse=True)

    acc = 0
    points = []
    for (y, x) in indexed:
        if acc + y > mass:
            break;
        acc += y
        points.append(x)

    points.sort()

    sections = []
    section = [points[0], points[0]]
    for point in points[1:]:
        if section[1] != (point - 1):
            sections.append(section)
            section = (point, point)
        else:
            section[1] = point

    sections.append(section)

    xs = np.linspace(xlim[0], xlim[1], len(ys))
    translated = [(xs[lb], xs[ub]) for (lb, ub) in sections]
    return translated

# A function to process fcs files given thru datafilelist as a list of glob handles
#
def extractFCSData(datafilelist, sortedidx, boxlabels):

    samplelist = [FCMeasurement(ID=curfile, datafile=curfile) for curfile in datafilelist]
    x_axis_key = 'FSC'
    filter_key = 'SSC'
    y_axis_key = 'FL1'
    gates = {}
    data = {}
    fig_title = 'BSB1 SG0'
    # define gates
    #gates['rrnB'] = fcm.PolyGate(numpy.array([[2,100],[2,3500], [3500,3500],[3500,2]]), (0,2))
    gates[x_axis_key+'0'] = ThresholdGate(150, x_axis_key, region='above')
    gates[x_axis_key+'1'] = ThresholdGate(500, x_axis_key, region='below')
    gates[filter_key] = ThresholdGate(3000, filter_key, region='below')
    gates[y_axis_key] = ThresholdGate(10, y_axis_key, region='above')

    gate_3d = gates[x_axis_key+'0'] & gates[x_axis_key+'1'] & gates[filter_key] & gates[y_axis_key] #CompositeGate(gates[x_axis_key], 'and', gates[y_axis_key])
    samplelist_gated = [sample.gate(gate_3d) for sample in samplelist]
    #data[fig_title] = (samplelist_gated, samplelist) #[sample.data[list(sample.channel_names)] for sample in samplelist]
    data[fig_title] = (samplelist_gated, samplelist_gated) #[sample.data[list(sample.channel_names)] for sample in samplelist]
    ##sortedidx = [0,1,2,3,4] #,4,5]
    ##boxlabels = ['m0 150m', 'm1 150m','m2 150m', 'overnite', '+ctrl']

    for item in data.items():
        curname = item[0]
        curdata = item[1][0]
        curdata_raw = item[1][1]
        print 'processing ' + curname

        graphs = {}

        indices = []
        scplot_list = []
        for i in range(0,len(curdata)):
            indices.append([2,0])
        #
        # ncols = 2
        # nrows = 3
        #
        # figure, ax = pylab.subplots(nrows, ncols, figsize=(12,15), sharex=True, sharey=True)
        #
        # figure.suptitle(curname)
        # ylabelstr = 'Fluorescence (AU)'
        # xlabelstr = 'Forward Scatter (AU)'

        # for i_unsorted, idx in enumerate(indices):
        #     print 'indices: ' + str(indices)
        #     print 'sortedidx: ' + str(sortedidx)
        #     i = sortedidx[i_unsorted]
        #     m = int(i_unsorted%nrows) # number of rows
        #     n = int(i_unsorted/nrows) # number of cols
        #     if (idx[0] != idx[1]):
        #         x = np.array([x_val/6.0 if x_val > 0 else 1.0 for x_val in curdata_raw[i][x_axis_key].values]) #:, idx[0]]
        #         y = np.array([y_val/35.0 if y_val > 0 else 1.0 for y_val in curdata_raw[i][y_axis_key].values]) #:, idx[1]]
        #         H,xedges,yedges = np.histogram2d(x,y,bins=[40,40]) #,range=[[min(x),max(x)],[min(y),max(y)]])
        #
        #     z_indices = [(np.clip(xedges.searchsorted(x_val)-1,0,len(xedges)-2),np.clip(yedges.searchsorted(y_val)-1,0,len(yedges)-2)) for (x_val,y_val) in zip(x,y)]
        #     z = [H[np.clip(xedges.searchsorted(x_val)-1,0,len(xedges)-2),np.clip(yedges.searchsorted(y_val)-1,0,len(yedges)-2)] for (x_val,y_val) in zip(x,y)]
        #     s=10
        #     edgecolors='none'
        #     savefile = curname + '_scatter_'+str(0)
        #     ax[m,n].set_yscale('log', ybase=3, nonposy='clip')
        #     ax[m,n].set_xscale('log', xbase=4, nonposx='clip')
        #     scplot = ax[m,n].scatter(x, y, c=np.log(z), s=s, cmap=cm.rainbow, edgecolors=edgecolors, vmin=0,vmax=7)
        #     scplot_list.append(scplot)
        #     pylab.setp([ax[m,n]], title=boxlabels[i]) # title=curdata[i].ID)
        #     ax[m,n].set_ylim(1, ax[m,n].get_ylim()[1])
        #     ax[m,n].set_xlim(1, ax[m,n].get_xlim()[1])
        #
        #     if m == nrows-1:
        #         ax[m,n].set_xlabel(xlabelstr)
        #     if n == 0: # first columns
        #         ax[m,n].set_ylabel(ylabelstr) #.channel_names[idx[1]])
        #
        # if savefile:
        #     cbar_ax = figure.add_axes([0.91,0.10,0.01,0.25])
        #     figure.colorbar(scplot_list[-1], cax=cbar_ax, label='log(cell count)')
        #     ##pylab.show()
            #pylab.savefig(savefile)

        # ncols = 2
        # nrows = 4

        # xlabelstr = 'Fluorescence (AU)'
        # pylab.xlabel(xlabelstr)

        cindices = range(len(curdata))
        jet = plt.get_cmap('rainbow')
        c_norm = colors.Normalize(vmin=0, vmax=cindices[-1])
        scalar_cmap = cmx.ScalarMappable(norm=c_norm, cmap=jet)

        boxplot_data = []
        hist_data = []
        hist_labels = []
        hist_bar_labels = []
        for idx, curcurdata in enumerate(curdata):
            y = curcurdata[y_axis_key].values #:, 2] # 2 is fluorescence
            y_raw = curdata_raw[idx][y_axis_key].values
            y_raw_adjusted = curdata_raw[idx][y_axis_key].values/curdata_raw[idx][x_axis_key]
            #y_filtered = np.array([math.log((y_val+35)/35.0,3) for y_val in y_raw if y_val>=100])
            #y_filtered_clipped = np.array([val for val in np.clip(y_filtered, 0, max(y_filtered)) if val >= 0])
            y_filtered_clipped = np.array([val for val in np.clip(y_raw_adjusted, 0, max(y_raw_adjusted)) if val >= 0.0])
            y_filtered_nonzero = np.array([val for val in y_filtered_clipped if val > 0])

            # m = int(idx%(nrows-1))+1 # number of rows
            # n = int(idx/(nrows-1)) # number of cols

            unused_x = pylab.linspace(min(y), max(y), 100)
            mu, sigma = 100, 15
            cmap = scalar_cmap.to_rgba(cindices[idx])
            #cur_ax = plt.subplot2grid((nrows, ncols), (m, n))
            #cur_ax.set_xlim([1,6])
            #bincount, bins, patches = cur_ax.hist(y_filtered_clipped, 50, normed=False, alpha=0.75, color=cmap) #, range=[1,10**2])

            #if max(bincount) < 100:
            #    cur_ax.set_ylim([0,100])
            #else:
            #    cur_ax.set_ylim([0,1000])
            #hist_data.append(sum(bincount[1:])/float(len(y_raw)))
            #hist_bar_labels.append(str(int(sum(bincount[1:])))+'/'+str(len(y_raw)))
            #hist_labels.append(str(boxlabels[idx]))
            #best_fit = mlab.normpdf(bins, mu, sigma)
            #cur_ax.plot(bins, best_fit, linewidth=1, color=cmap, label=boxlabels[idx]) #label=curcurdata.ID)
            #cur_ax.legend(loc='upper right', fontsize='large')

            #if n == 0:
            #    cur_ax.set_ylabel('cell count')
            #if m == 3:
            #    cur_ax.set_xlabel('Fluorescence (AU)') #, labelpad=-5)

            boxplot_data.append(y_filtered_clipped)

    return boxplot_data#, hist_data, hist_labels, hist_bar_labels

        # cur_ax = plt.subplot2grid((nrows, ncols), (0, 0))
        # cur_ax.set_zorder(0)
        # cur_ax.boxplot(boxplot_data[:-1], labels=boxlabels[:-1])
        # cur_ax.set_ylabel('Fluorescence\n(AU)')
        # cur_ax.set_ylim([0,5])

        # barwidth = 0.35
        # cur_ax = plt.subplot2grid((nrows, ncols), (0, 1))
        # cur_ax.set_zorder(0)
        # x_series = np.arange(len(hist_data))+barwidth
        # cur_ax.bar(x_series[:-1], hist_data[:-1], width=barwidth)
        # cur_ax.set_xticks(x_series[:-1] + barwidth/2.)
        # cur_ax.set_xticklabels(hist_labels[:-1])
        # cur_ax.set_ylabel('Fraction of\n fluorescent cells')
        # cur_ax.set_ylim(0, 1.0)
        # for i, (xval, yval) in enumerate(zip(x_series[:-1], hist_data[:-1])):
        #     cur_ax.text(xval, yval+0.01, hist_bar_labels[i])

        ##pylab.show()


