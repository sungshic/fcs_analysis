#import fcm
#import fcm.graphics as graph
#from fcm.graphics.util import bilinear_interpolate
from FlowCytometryTools import FCMeasurement
from FlowCytometryTools import ThresholdGate
from glob import glob
import numpy as np
import matplotlib.pylab as pylab
import matplotlib.mlab as mlab
import matplotlib.cm as cmx
from matplotlib import cm
import matplotlib.colors as colors
from scipy import histogram
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import math

data = {}

#data['rrnB'] = [fcm.loadFCS(x) for x in sorted(glob('rrnB*.FCS')+glob('rrnB*.fcs'))]
#data['BSB1'] = [fcm.loadFCS(x) for x in sorted(glob('BSB1*.FCS')+glob('BSB1*.fcs'))]
#data['PBS'] = [fcm.loadFCS(x) for x in sorted(glob('PBS*.FCS')+glob('PBS*.fcs'))]
#data['SynWT'] = [fcm.loadFCS(x) for x in sorted(glob('Syn WT*.FCS')+glob('Syn WT*.fcs'))]
#data['PSpaS'] = [fcm.loadFCS(x) for x in sorted(glob('PspaS*.FCS')+glob('PspaS*.fcs'))]
#data['PliaG'] = [fcm.loadFCS(x) for x in sorted(glob('PliaG*.FCS')+glob('PliaG*.fcs'))]
#datafilepattern = 'data/29may/bsb1_evot2_1_m*'
datafilepattern = 'data/14july/evot2_m0*150*'
datafilepattern1 = 'data/14july/evot2_m1*150*'
datafilepattern2 = 'data/14july/evot2_m2*150mins*'
datafilepattern3 = 'data/14july/evot2_m3*150mins*'
datafilepattern4 = 'data/14july/evot2_overnite*'
#datafilepattern = 'data/29may/bsb1_WT_m*'
posctrl = 'data/14july/xfbeads*'
#datafilepattern = 'data/06june/bsb1_evot2_m*'
#posctrl = 'data/06june/fbeads*'
#fig_title = 'BSB1 EVOt2'
#fig_title = 'BSB1 WT'
fig_title = 'BSB1 SG0'
datafilelist = sorted(glob(datafilepattern1)+glob(datafilepattern2)+glob(datafilepattern3)+glob(datafilepattern4)+glob(datafilepattern)+glob(posctrl))
#data['WT'] = [fcm.loadFCS(x) for x in sorted(glob(datafiledir+'bsb1_WT*'))]
samplelist = [FCMeasurement(ID=curfile, datafile=curfile) for curfile in datafilelist]
x_axis_key = 'FSC'
filter_key = 'SSC'
y_axis_key = 'FL1'
gates = {}
# define gates
#gates['rrnB'] = fcm.PolyGate(numpy.array([[2,100],[2,3500], [3500,3500],[3500,2]]), (0,2))
gates[x_axis_key] = ThresholdGate(50, x_axis_key, region='above')
gates[filter_key] = ThresholdGate(3000, filter_key, region='below')
gates[y_axis_key] = ThresholdGate(500, y_axis_key, region='above')

gate_3d = gates[x_axis_key] & gates[filter_key] & gates[y_axis_key] #CompositeGate(gates[x_axis_key], 'and', gates[y_axis_key])
samplelist_gated = [sample.gate(gate_3d) for sample in samplelist]
#data['WT'] = samplelist_gated #[sample.data[list(sample.channel_names)] for sample in samplelist]
data[fig_title] = (samplelist_gated, samplelist) #[sample.data[list(sample.channel_names)] for sample in samplelist]
#data[fig_title] = (samplelist_gated, samplelist_gated) #[sample.data[list(sample.channel_names)] for sample in samplelist]

#sortedidx= [0,3,1,2,4,7,5,6]
#sortedidx = [0, 12, 21, 3, 6, 9, 15, 18, 1, 13, 22, 4, 7, 10, 16, 19, 2, 14, 23, 5, 8, 11, 17, 20]
sortedidx = [0,1,2,3,4,5]
boxlabels = ['m0', 'm1', 'm2', 'm3', 'overnite', '+ctrl']

majorxLocator = MultipleLocator(1000)
majorFormatter = FormatStrFormatter('%d')
minorxLocator = MultipleLocator(500)
majoryLocator = MultipleLocator(2000)
minoryLocator = MultipleLocator(1000)
majorcountLocator = MultipleLocator(500)
minorcountLocator = MultipleLocator(250)

for item in data.items():
	curname = item[0]
	curdata = item[1][0]
	curdata_raw = item[1][1]
	print 'processing ' + curname
	#for i in range(0,len(curdata)):
	#	gates['rrnB'].gate(curdata[i])

	graphs = {}

	indices = []
	scplot_list = []
	#import ipdb; ipdb.set_trace()
	for i in range(0,len(curdata)):
		indices.append([2,0])
	#for i in range(0,len(curdata)):
	#graphs[curname+'_scatter'] = graph.pseudocolor(curdata, indices, 4,2, display=False)
	#graphs[curname+'_scatter'].savefig(curname+'_scatter_'+str(0))

	ncols = 2
	nrows = 3

	figure, ax = pylab.subplots(nrows, ncols, figsize=(12,15), sharex=True, sharey=True)
	#figure.tight_layout() # more space in between subplots

	figure.suptitle(curname)
	ylabelstr = 'Fluorescence (AU)'
	xlabelstr = 'Forward Scatter (AU)'
	
	#pylab.ylabel('Size')
    	for i_unsorted, idx in enumerate(indices):
	#ax = pylab.subplot(nrows, ncols, i + 1)
	#ax[i] = figure.add_subplot(gs[i],sharex=True,sharey=True)
		print 'indices: ' + str(indices)
		print 'sortedidx: ' + str(sortedidx)
		i = sortedidx[i_unsorted]
		m = int(i_unsorted%nrows) # number of rows
		n = int(i_unsorted/nrows) # number of cols
		if (idx[0] != idx[1]):
		    # hotfix by spark to add [i]
		    #x = np.array([math.log(x_val*60.0, 3) if x_val != 0 else x_val for x_val in curdata[i][x_axis_key].values]) #:, idx[0]]
		    #y = np.array([math.log(y_val*450.0, 3) if y_val != 0 else y_val for y_val in curdata[i][y_axis_key].values]) #:, idx[1]]
		    x = np.array([x_val/6.0 if x_val > 0 else 1.0 for x_val in curdata_raw[i][x_axis_key].values]) #:, idx[0]]
		    y = np.array([y_val/35.0 if y_val > 0 else 1.0 for y_val in curdata_raw[i][y_axis_key].values]) #:, idx[1]]
                    H,xedges,yedges = np.histogram2d(x,y,bins=[40,40]) #,range=[[min(x),max(x)],[min(y),max(y)]]) 
                    #H,xedges,yedges = np.histogram2d(x,y) 
		
		    #if 'c' not in kwargs:
		z_indices = [(np.clip(xedges.searchsorted(x_val)-1,0,len(xedges)-2),np.clip(yedges.searchsorted(y_val)-1,0,len(yedges)-2)) for (x_val,y_val) in zip(x,y)]
		#z = [H[np.clip(xedges.searchsorted(x_val)-1,0,len(xedges)-2),np.clip(yedges.searchsorted(y_val)-1,0,len(yedges)-2)] for (x_val,y_val) in zip(curdata[i][x_axis_key].values,curdata[i][y_axis_key].values)]
		z = [H[np.clip(xedges.searchsorted(x_val)-1,0,len(xedges)-2),np.clip(yedges.searchsorted(y_val)-1,0,len(yedges)-2)] for (x_val,y_val) in zip(x,y)]
		#z = bilinear_interpolate(x, y)
		#z = x*np.exp(-x**2-y**2)
			#pylab.scatter(x, y, c=z, s=s, edgecolors=edgecolors, **kwargs)
		s=10
		edgecolors='none'
		#edgecolors='color'
		savefile = curname + '_scatter_'+str(0)
                #import ipdb; ipdb.set_trace()
		#ax[m,n].scatter(x, y, c=z, s=s, edgecolors=edgecolors)
		ax[m,n].set_yscale('log', ybase=3, nonposy='clip')
		ax[m,n].set_xscale('log', xbase=4, nonposx='clip')
		#ax[m,n].scatter(x, y, c=z, s=s, edgecolors=edgecolors,cmap=cm.jet)
		#ax[m,n].scatter(x, y, c=z, s=s, cmap=cm.jet)
		#ax[m,n].scatter(x, y, c=z, s=s, cmap='Pastel2',edgecolors=edgecolors)
		scplot = ax[m,n].scatter(x, y, c=np.log(z), s=s, cmap=cm.rainbow, edgecolors=edgecolors, vmin=0,vmax=7)
                scplot_list.append(scplot)
		pylab.setp([ax[m,n]], title=boxlabels[i]) # title=curdata[i].ID)
		ax[m,n].set_ylim(1, ax[m,n].get_ylim()[1])
		ax[m,n].set_xlim(1, ax[m,n].get_xlim()[1])

		#ax[nrows,n].legend()
		    #else:
			#pylab.scatter(x, y, s=s, edgecolors=edgecolors, **kwargs)
		#	ax[m,n].scatter(x, y, s=s, edgecolors=edgecolors, **kwargs)
		    #if isinstance(idx[0], str):
			#pylab.xlabel(idx[0])
		#	ax[m,n].set_xlabel(idx[0])
		#    else:
			#pylab.xlabel(fcm[i].channels[idx[0]])
		#ax[m,n].set_xlabel(curdata[i].channels[idx[0]])
		#ax[m,n].set_xlabel(curdata[i].name) #channels[idx[0]])
                if m == nrows-1:
		#ax[m,n].set_xlabel(xlabelstr) #curdata[i].channels[idx[0]])
		#ax[m,n].set_xlabel(curdata[i][x_axis_key].name) 
                    ax[m,n].set_xlabel(xlabelstr) 
		#ax[m,n].set_ylabel(curdata[i][y_axis_key].name) #.channel_names[idx[1]])
                if n == 0: # first columns
                    ax[m,n].set_ylabel(ylabelstr) #.channel_names[idx[1]])
		##ax[m,n].xaxis.set_major_locator(majorxLocator)
		##ax[m,n].xaxis.set_major_formatter(majorFormatter)
		##ax[m,n].xaxis.set_minor_locator(minorxLocator)

		#    if isinstance(idx[1], str):
			#pylab.ylabel(idx[1])
		#	ax[m,n].set_ylabel(idx[1])
		#    else:
			#pylab.ylabel(fcm[i].channels[idx[1]])
		##ax[m,n].yaxis.set_major_locator(majoryLocator)
		##ax[m,n].yaxis.set_major_formatter(majorFormatter)
		##ax[m,n].yaxis.set_minor_locator(minoryLocator)
		### spark hotfix
		#pylab.xticks([])
		#pylab.yticks([])
		#ax[m,n].set_ylim([0,3000])
		#ax[m,n].set_xlim([0,4000])

		#ax.get_yaxis().set_label_coords(arange(0,3000))
		#pylab.axis('equal')

	#import ipdb; ipdb.set_trace()
    	if savefile:
		cbar_ax = figure.add_axes([0.91,0.10,0.01,0.25])
		figure.colorbar(scplot_list[-1], cax=cbar_ax, label='log(cell count)')
		pylab.show()

		#pylab.savefig(savefile)


	
	ncols = 2
	nrows = 4
	#figure, ax = pylab.subplots(nrows,ncols) #nrows, ncols, sharex=True, sharey=True)
	#ax_list = []
	#ax_list.append(plt.subplot2grid((4,3), (0,0))
	
	xlabelstr = 'Fluorescence (AU)'
	pylab.xlabel(xlabelstr)
	#pylab.ylabel('Size')
	#colormap = plt.cm.rainbow(np.linspace(0,1,len(curdata)))
	cindices = range(len(curdata))
	jet = cm = plt.get_cmap('rainbow')
	c_norm = colors.Normalize(vmin=0, vmax=cindices[-1])
	scalar_cmap = cmx.ScalarMappable(norm=c_norm, cmap=jet)
	
	boxplot_data = []
	hist_data = []
	hist_labels = []
	hist_bar_labels = []
	for idx, curcurdata in enumerate(curdata):
		y = curcurdata[y_axis_key].values #:, 2] # 2 is fluorescence
		y_raw = curdata_raw[idx][y_axis_key].values
		#y = np.array([math.log(y_val*450.0, 3) if y_val != 0 else y_val for y_val in curcurdata[y_axis_key].values]) #:, idx[1]]
		#h1, b1 = histogram(list(y), bins=2)
		#y_filtered = [y_val for y_val in y if y_val!=0]
		#y_filtered = np.array([math.log(y_val/35.0,3) for y_val in y if y_val>0])
		#y_filtered_clipped = np.array([val for val in np.clip(y_filtered, 0, max(y_filtered)) if val >= 1])
		y_filtered = np.array([math.log((y_val+35)/35.0,3) for y_val in y_raw if y_val>=0])
		y_filtered_clipped = np.array([val for val in np.clip(y_filtered, 0, max(y_filtered)) if val >= 0])
		y_filtered_nonzero = np.array([val for val in y_filtered_clipped if val > 0])
		
		m = int(idx%(nrows-1))+1 # number of rows
		n = int(idx/(nrows-1)) # number of cols
		
		#b1 = (b1[:-1] + b1[1:]) / 2.0
		unused_x = pylab.linspace(min(y), max(y), 100)
		#import ipdb; ipdb.set_trace()
		#ax.plot(b1, h1, label=curcurdata.ID)
		mu, sigma = 100, 15
		cmap = scalar_cmap.to_rgba(cindices[idx])
		#ax.plot(bins, best_fit, linewidth=1, color=cmap, label=curcurdata.ID)
		cur_ax = plt.subplot2grid((nrows, ncols), (m, n))
		#bincount, bins, patches = cur_ax.hist(y_filtered, 50, normed=False, alpha=0.75, color=cmap, range=[0,4500])
		#cur_ax.set_yscale('log', nonposy='clip')
		#cur_ax.set_xlim([0,4500])
		cur_ax.set_xlim([1,6])
		#bincount, bins, patches = cur_ax.hist(y_filtered_clipped, 50, normed=False, alpha=0.75, color=cmap) #, range=[1,10**2])
		bincount, bins, patches = cur_ax.hist(y_filtered_clipped, 50, normed=False, alpha=0.75, color=cmap) #, range=[1,10**2])

                if max(bincount) < 100:
		    cur_ax.set_ylim([0,100])
                else:
                    cur_ax.set_ylim([0,1000])
		#cur_ax.set_xscale('log', ybase=3, nonposy='clip')
		#cur_ax.set_xlim([1,100])
		#bincount, bins, patches = ax[m,n].hist(y, 50, normed=False, alpha=0.75, color=cmap)
		#hist_data.append(sum(bincount)/float(len(y_raw)))
		hist_data.append(sum(bincount[1:])/float(len(y_raw)))
		#hist_labels.append(str(int(sum(bincount)))+'/'+str(len(y))+'\n('+str(boxlabels[idx])+')')
                hist_bar_labels.append(str(int(sum(bincount[1:])))+'/'+str(len(y_raw))) 
		hist_labels.append(str(boxlabels[idx]))
		best_fit = mlab.normpdf(bins, mu, sigma)
		#ax[m,n].plot(bins, best_fit, linewidth=1, color=cmap, label=curcurdata.ID)
		#ax.legend(loc='upper left', fontsize='small')
		#ax[m,n].legend(loc='upper right', fontsize='small')
		cur_ax.plot(bins, best_fit, linewidth=1, color=cmap, label=boxlabels[idx]) #label=curcurdata.ID)
		cur_ax.legend(loc='upper right', fontsize='large')
		#cur_ax.plot(bins, best_fit, linewidth=1, color=cmap) #label=curcurdata.ID)
		#pylab.setp([cur_ax], title=boxlabels[idx]) # title=curdata[i].ID)

                #ax[m,n].set_ylabel('cell count')
                if n == 0: 
                    cur_ax.set_ylabel('cell count')
                if m == 3:
                    cur_ax.set_xlabel('Fluorescence (AU)') #, labelpad=-5)
                #majorFormatter = FormatStrFormatter('%d')
                #majoryLocator = MultipleLocator(100)
                #minoryLocator = MultipleLocator(50)
                #ax[m,n].yaxis.set_major_locator(majorcountLocator)
                #ax[m,n].yaxis.set_major_formatter(majorFormatter)
                #ax[m,n].yaxis.set_minor_locator(minorcountLocator)
		#cur_ax.set_ylim([0,450])
		#cur_ax.margins(0.05)
			
                boxplot_data.append(y_filtered_clipped)

	#cur_ax = plt.subplot2grid((nrows, ncols), (nrows-1, 0), colspan=ncols)
	cur_ax = plt.subplot2grid((nrows, ncols), (0, 0))
	#cur_ax.set_ymargin(1.0)
	cur_ax.set_zorder(0)
        cur_ax.boxplot(boxplot_data[:-1], labels=boxlabels[:-1])
	cur_means = [np.mean(data) for data in boxplot_data[:-1]]
	print 'cur_means: ' + str(cur_means)
	cur_ax.plot(np.arange(len(cur_means)) + 1, cur_means, 'rs')
        cur_ax.set_ylabel('Fluorescence\n(AU)')
	cur_ax.set_ylim([0,5])

	#import ipdb; ipdb.set_trace()
	barwidth = 0.35
	cur_ax = plt.subplot2grid((nrows, ncols), (0, 1))
	#cur_ax.set_ymargin(1.0)
	#cur_ax.margins(1.0)
	cur_ax.set_zorder(0)
	x_series = np.arange(len(hist_data))+barwidth
	cur_ax.bar(x_series[:-1], hist_data[:-1], width=barwidth)
	cur_ax.set_xticks(x_series[:-1] + barwidth/2.)
	cur_ax.set_xticklabels(hist_labels[:-1])
        cur_ax.set_ylabel('Fraction of\n fluorescent cells')
	cur_ax.set_ylim(0, 1.0)
        for i, (xval, yval) in enumerate(zip(x_series[:-1], hist_data[:-1])):
            #cur_ax.text(xval, yval+0.01, '%.3f'%yval)
            cur_ax.text(xval, yval+0.01, hist_bar_labels[i])

	#plt.tight_layout(rect=(0,0,1,1))
	pylab.show()
	#pylab.savefig(curname+'_hist')
		
	#graphs[curname+'_hist'] = graph.hist(curdata, 2, display=False)
	#hist_axes = graphs[curname+'_hist'].add_subplot(1,1,1)
	#hist_axes.set_xlabel('fluorescence (AU)')
	#hist_axes.set_ylabel('cell count')
	#graphs[curname+'_hist'].savefig(curname+'_hist')


