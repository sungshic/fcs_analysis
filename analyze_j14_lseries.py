__author__='spark'
#import fcm
#import fcm.graphics as graph
#from fcm.graphics.util import bilinear_interpolate
from FlowCytometryTools import FCMeasurement
from FlowCytometryTools import ThresholdGate
from glob import glob
import numpy as np
import matplotlib.pylab as pylab
import matplotlib.mlab as mlab
import matplotlib.cm as cmx
from matplotlib import cm
import matplotlib.colors as colors
from scipy import histogram
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import math

from fcs_data_util import extractFCSData, calcBayesianCredibleRegion, calcConfidenceInterval

strain_list = ['evot2'] #, 'sg0']
media_list = ['m0', 'm1', 'm2', 'm3']
min_list = ['150', '210', '270', '330', '390']
data_path = 'data/14july/'

data = []

for cur_strain in strain_list:
    for cur_medium in media_list:
        #data['rrnB'] = [fcm.loadFCS(x) for x in sorted(glob('rrnB*.FCS')+glob('rrnB*.fcs'))]
        #data['BSB1'] = [fcm.loadFCS(x) for x in sorted(glob('BSB1*.FCS')+glob('BSB1*.fcs'))]
        #data['PBS'] = [fcm.loadFCS(x) for x in sorted(glob('PBS*.FCS')+glob('PBS*.fcs'))]
        #data['SynWT'] = [fcm.loadFCS(x) for x in sorted(glob('Syn WT*.FCS')+glob('Syn WT*.fcs'))]
        #data['PSpaS'] = [fcm.loadFCS(x) for x in sorted(glob('PspaS*.FCS')+glob('PspaS*.fcs'))]
        #data['PliaG'] = [fcm.loadFCS(x) for x in sorted(glob('PliaG*.FCS')+glob('PliaG*.fcs'))]
        #datafilepattern = 'data/29may/bsb1_evot2_1_m*'
        time_series = []
        for cur_min in min_list:
            datafilepattern = data_path+cur_strain+'_'+cur_medium+'*'+cur_min+'mins*'
            datafilepattern4 = data_path+cur_strain+'_overnite*'
            #datafilepattern = 'data/29may/bsb1_WT_m*'
            posctrl = data_path+'xfbeads*'
            #datafilepattern = 'data/06june/bsb1_evot2_m*'
            #posctrl = 'data/06june/fbeads*'
            #fig_title = 'BSB1 EVOt2'
            #fig_title = 'BSB1 WT'
            fig_title = 'BSB1 SG0'
            datafilelist = sorted(glob(datafilepattern)+glob(datafilepattern4)+glob(posctrl))

            sortedidx = [0,1,2]
            boxlabels = [cur_medium+' '+cur_min, 'overnite','pos_ctrl']

            #boxplot_data, hist_data, hist_labels, hist_bar_labels = extractFCSData(datafilelist, sortedidx, boxlabels)
            boxplot_data = extractFCSData(datafilelist, sortedidx, boxlabels)
            sigma = np.std(boxplot_data[0])
            #data_cr = calcBayesianCredibleRegion(boxplot_data[0],sigma,frac=0.15)
            data_cr = calcConfidenceInterval(boxplot_data[0],frac=0.95)
            #time_series.append((boxplot_data[0].mean(), data_cr))
            time_series.append(boxplot_data[0])
            #time_series.append(boxplot_data[0])
        if cur_medium == 'm2':
            time_series.append(boxplot_data[1])
        data.append(time_series)


ncols = 1
nrows = 1

#cur_ax = plt.subplot2grid((nrows, ncols), (nrows-1, 0), colspan=ncols)
cur_ax = plt.subplot2grid((nrows, ncols), (0, 0))
#cur_ax.set_ymargin(1.0)
cur_ax.set_zorder(0)

min_num_list = np.array([int(minstr) for minstr in min_list])
min_offset = -1.8
plot_handle_list = []
for cur_timeseries in data:
    #mean_list, cr_list = zip(*cur_timeseries)
    #print cr_list
    #cur_ax.errorbar(min_num_list+min_offset, mean_list, yerr=cr_list, fmt='-o')
    box_pos = np.arange(len(cur_timeseries))*5.0

    cur_ax.boxplot(cur_timeseries, widths=0.6, positions=box_pos+min_offset) #, labels=min_list) #boxlabels[:-1])
    cur_means = [np.mean(tdata) for tdata in cur_timeseries] #boxplot_data[:-1]]
    print 'cur_means: ' + str(cur_means)
    #cur_ax.plot(np.arange(len(cur_means)) + 1, min_num_list+min_offset, cur_means, 'rs')
    plot_handle, = cur_ax.plot(box_pos+min_offset, cur_means, '-s')
    plot_handle_list.append(plot_handle)

    min_offset += 1

cur_ax.legend(plot_handle_list, media_list)
cur_ax.set_ylabel('Fluorescence (AU)')
cur_ax.set_ylim([0,5])
cur_ax.set_xlim([-5,35])
box_pos = np.arange(len(data[2]))*5.0
cur_ax.set_xticks(box_pos) #, min_list)
cur_ax.set_xticklabels(min_list+['overnight'])
cur_ax.set_xlabel('Time (min)')

#plt.tight_layout(rect=(0,0,1,1))
pylab.show()
#pylab.savefig(curname+'_hist')

#graphs[curname+'_hist'] = graph.hist(curdata, 2, display=False)
#hist_axes = graphs[curname+'_hist'].add_subplot(1,1,1)
#hist_axes.set_xlabel('fluorescence (AU)')
#hist_axes.set_ylabel('cell count')
#graphs[curname+'_hist'].savefig(curname+'_hist')


